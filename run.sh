#!/bin/bash -l
#SBATCH --job-name=bench1
##SBATCH --mail-user=mikst@fysik.dtu.dk
##SBATCH --mail-type=START,END
#SBATCH --partition=xeon24
#SBATCH --output=%j.out
#SBATCH --time=6:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=24
#SBATCH --ntasks-per-node=24
#SBATCH --mem=250G
module purge
module load GPAW
mpiexec gpaw-python t1.py
